/*
 * Dara Klinkner
 * George Fox University
 * ENGR 152
 * 
 * HW 8 Task 2
 * Using a piezo buzzer and a photoresistor
 * 
 */

// ********************************************
//      Variable Declarations
// ********************************************

int sensorValue; // a place to put the photoresistor value
int sensorLow = 1023;
int sensorHigh = 0;

const int ledPin = 13; // onboard LED
 

// ********************************************
//      Setup Code 
// ********************************************
 void setup() {

  pinMode(ledPin, OUTPUT); // sets onboard LED to be a blinky
  digitalWrite(ledPin, HIGH); // makes it high all the time

  while (millis() < 5000) { // makes a 5 second "delay" for initialization
    
    sensorValue = analogRead(A0); // reads the photoresistor
    
    if (sensorValue > sensorHigh) { // tests the highest value of the photoresistor and resets
      sensorHigh = sensorValue;
    }

    if (sensorValue < sensorLow) { // tests the lowest possible value of the photoresistor and sets 
      sensorLow = sensorValue;
    }
  }

  digitalWrite(ledPin, LOW); // turns off the onboard LED

}

// ********************************************
//      Main Code 
// ********************************************
void loop() {
  
  sensorValue = analogRead(A0); // reads in the sensor's value
  
  int pitch = map(sensorValue, sensorLow, sensorHigh, 50, 4000); // creates a variable with the piezo buzzer pitch
  
  tone(8, pitch, 20); // what pin, sound to play, and how long to play the note

  delay(10);

}
