/*
 * HW #4 Task #4 - Project 3 in the arduino book
 * 
 * Dara Klinkner, February 17th, 2019
 * 
 * ENGR 152, George Fox University
 * 
 * 
 */

 // Defining the variables to be used for the program
 
const int sensorPin = A0; // analog input pin for the temperature sensor
const float baselineTemp = 20.0; // temperature variable with initial value
int redLED1 = 2;
int redLED2 = 3;
int redLED3 = 4;

//=========================================================================
//          Setup
//=========================================================================

void setup() {

 Serial.begin(9600); // start a serial port so we can read the inputs and outputs of the temperature sensor and our program
 pinMode(redLED1, OUTPUT); // initialize the pins the LEDs are connected to as outputs
 pinMode(redLED2, OUTPUT);
 pinMode(redLED3, OUTPUT);
 
}


//=========================================================================
//          Main
//=========================================================================
void loop(){

int sensorVal = analogRead(sensorPin); // reads the temperature sensors voltage and stores it in sensorVal

Serial.print("Sensor Value: "); // prints the stuff in quotes into the serial monitor
Serial.print(sensorVal); // prints the raw sensor value after the above

// Now we'll convert our raw reading into a voltage
float voltage = (sensorVal/1024.0) * 5.0;

// We'll print the voltage, like we did with the sensor value above
Serial.print(", Voltage: ");
Serial.print(voltage);

Serial.print(", degrees C: "); // prints after the voltage, showing the relationship

// convert the voltage calculated to temperature displayed in Celsius
float temperature = (voltage - 0.5) * 100;
Serial.println(temperature); // prints our newly calculated temperature value

// Now we'll have the LEDs light up based on our measurements.

if(temperature < baselineTemp){
  digitalWrite(redLED1, LOW);
  digitalWrite(redLED2, LOW);
  digitalWrite(redLED3, LOW);
} else if (temperature >= baselineTemp+2 && temperature < baselineTemp+4){
  digitalWrite(redLED1, HIGH);
  digitalWrite(redLED2, LOW);
  digitalWrite(redLED3, LOW);
} else if (temperature >= baselineTemp+4 && temperature < baselineTemp+6){
  digitalWrite(redLED1, HIGH);
  digitalWrite(redLED2, HIGH);
  digitalWrite(redLED3, LOW);
} else if (temperature >= baselineTemp+6){
  digitalWrite(redLED1, HIGH);
  digitalWrite(redLED2, HIGH);
  digitalWrite(redLED3, HIGH);
}

delay(1); // delays the program for a second before looping back

}
