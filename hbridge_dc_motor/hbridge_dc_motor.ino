/*
Dara Klinkner
George Fox University
ENGR152
HW 8 Task 5
3/16/2019
*/

// *********************************
//  Variables
// *********************************

const int controlPin1 = 2; // connected to pin 7 H-bridge
const int controlPin2 = 3; // connected to pin 2 H-bridge
const int enablePin = 9;   // connected to pin 1 H-bridge
const int directionSwitchPin = 4;  // connected to the switch for direction
const int onOffSwitchStateSwitchPin = 5; // connected to the switch for turning the motor on and off
const int potPin = A0;  // connected to the potentiometer's output

int onOffSwitchState = 0;  // on/off switch
int previousOnOffSwitchState = 0; // previous state of the on/off switch
int directionSwitchState = 0;  // current state of the direction switch
int previousDirectionSwitchState = 0;  // previous state of the direction switch

int motorEnabled = 0; // Turns the motor on/off
int motorSpeed = 0; // motor speed
int motorDirection = 1; // current direction of the motor

// *********************************
//  Setup Code
// *********************************

void setup() {
  // initialize the inputs and outputs
  pinMode(directionSwitchPin, INPUT);
  pinMode(onOffSwitchStateSwitchPin, INPUT);
  pinMode(controlPin1, OUTPUT);
  pinMode(controlPin2, OUTPUT);
  pinMode(enablePin, OUTPUT);

  // set the enable pin LOW to start
  digitalWrite(enablePin, LOW);
}

// *********************************
//  Main loop
// *********************************
void loop() {
  
  // is the switch on or off?
  onOffSwitchState = digitalRead(onOffSwitchStateSwitchPin);
  delay(1);

  // read the value of the direction switch
  directionSwitchState = digitalRead(directionSwitchPin);

  motorSpeed = analogRead(potPin) / 4; // read the pot value and make it PWM
  
  if (onOffSwitchState != previousOnOffSwitchState) { // if the on/off button changed state
    
    if (onOffSwitchState == HIGH) {  // change the value of motorEnabled if pressed
      
      motorEnabled = !motorEnabled;
    }
  }
  
  if (directionSwitchState != previousDirectionSwitchState) { // if there was a change in direction from the pot
    
    if (directionSwitchState == HIGH) { // change the value of motorDirection if pressed
      motorDirection = !motorDirection;
    }
  }

  if (motorDirection == 1) { // if direction changed, talk to the H-bridge so we can change the motor
    digitalWrite(controlPin1, HIGH);
    digitalWrite(controlPin2, LOW);
  } else {
    digitalWrite(controlPin1, LOW);
    digitalWrite(controlPin2, HIGH);
  }

  // if the motor is supposed to be on
  if (motorEnabled == 1) {
    
    analogWrite(enablePin, motorSpeed); // PWM the enable pin to vary the speed
  } else { // the motor should be off otherwise
    
    analogWrite(enablePin, 0);
  }

  previousDirectionSwitchState = directionSwitchState; // save the current on/off switch state as the previous
  previousOnOffSwitchState = onOffSwitchState; // same with the off/on
}
