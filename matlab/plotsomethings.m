%  Dara Klinkner
%  April 9th, 2019
%  George Fox University, ENGR 152
% 
%  First time plotting with matlab.

clear;

% Creates arrays / 1xX matrices
Time = [8 9 11 15 80 87 90 95 103 110];
Volts = [6 .7 1 9 5 10 15 29 17 3];

% plots the values we have in the matrices
plot(Time,Volts,'m--') 

% labels the x and y axes
xlabel('Time')
ylabel('Volts')

% gives a title to the plot
title('Voltage over Time')

% labels the voltage as pink
legend('Volts = Pink')

% gives the plot a graph
grid on
