% Dara Klinkner
% George Fox University
% ENGR 152

% A program that prints the sin of an angle in radians

angle = input('Enter the angle: ');

printWith = input('(r)adians (the default) or (d)egrees: ', 's');


if printWith == 'd'
    disp('the sin is: ')
    sin = sind(angle);
    disp(sin)
else
    disp('the sin is: ')
    sin = sin(angle);
    disp(sin)
end
