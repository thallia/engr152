%  Dara Klinkner
%  April 9th, 2019
%  George Fox University, ENGR 152
%  
%  This script will be calculating the volume of a rectangular cube, based
%  on user input, and print your name as well as the volume.
%  width * length * height
%  Units: cm

length = input('Enter the length of the rectangle: ');
width = input('Enter the width of the rectangle: ');
height = input('Enter the height of the rectangle: ');
name = input('Enter your name: ', 's');

volume = (length * width * height);

disp('Your name is: ')
disp(name)
disp('Volume of rectangle:')
disp(volume)


