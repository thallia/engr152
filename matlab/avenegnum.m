% Dara Klinkner
% George Fox University
% ENGR 152

% a program that prints the average of negative numbers.

negInput = [];
inputnum = input('Enter a negative number: ');

while inputnum <= 0
     negInput = [negInput inputnum];
     inputnum = input('Enter a negative number: ');
end
avg = mean(negInput);
disp('the average is: ')
disp(avg)