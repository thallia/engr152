/*
 * Dara Klinkner
 * ENGR 152, George Fox University
 * HW #5 Task #3
*/

// ====================================================
//        Libraries & Variables
// ====================================================

#include <LiquidCrystal.h> // includes library for the display we're using
LiquidCrystal lcd(12, 11, 5, 4, 3, 2); // tells the Arduino what pins are used to communicate with the LCD screen

const int sensorPin = A0; // analog input pin for the temperature sensor
const float baselineTemp = 20.0; // temperature variable with initial value


// ====================================================
//        Setup Code
// ====================================================
void setup() {
  
  lcd.begin(16, 2); // turns the LCD screen on

}


// ====================================================
//        Main Code
// ====================================================
void loop() {

  int sensorVal = analogRead(sensorPin); // reads the temperature sensors voltage and stores it in sensorVal
  
  // Now we'll convert our raw reading into a voltage
  float voltage = (sensorVal/1024.0) * 5.0;

  // convert the voltage calculated to temperature displayed in Celsius
  float temperature = (voltage - 0.5) * 100;

  lcd.setCursor(0, 0); // sets display to first line
  lcd.print("Degrees C "); // prints the prefix to the temperature
  lcd.print(temperature); // prints our newly calculated temperature value
  

}
