
/*
 * Dara Klinkner, George Fox University
 * ENGR 152
 * HW #6 Task #2
 * Arduino Project #4
 * 
 */

// ***************************************************
//         Initializing Variables
// ***************************************************

const int greenLEDPin = 9; // initializes RGB LED pins
const int redLEDPin = 11;
const int blueLEDPin = 10;

const int redSensorPin = A0; // initializes the phototransistor pins
const int greenSensorPin = A1;
const int blueSensorPin = A2;

int redValue = 0; // initializes variables where we'll put the read data
int greenValue = 0;
int blueValue = 0;

int redSensorValue = 0;
int greenSensorValue = 0;
int blueSensorValue = 0;


// ***************************************************
//         Setup Code
// ***************************************************
void setup() {

  // setup the serial monitor
  Serial.begin(9600);

  // sets up the RGB LED pins as outputs so we can have color in our LEDs
  pinMode(greenLEDPin, OUTPUT);
  pinMode(redLEDPin, OUTPUT);
  pinMode(blueLEDPin, OUTPUT);


}


// ***************************************************
//         Main Code Loop
// ***************************************************
void loop() {

  // reads whether or not we have any values on the sensors
  redSensorValue = analogRead(redSensorPin);
  delay(5);
  greenSensorValue = analogRead(greenSensorPin);
  delay(5);
  blueSensorValue = analogRead(blueSensorPin);

  // displays the raw values read from the sensors
  Serial.print("\n Raw Sensor Values \n Red: ");
  Serial.print(redSensorValue);
  Serial.print("\n Green: ");
  Serial.print(greenSensorValue);
  Serial.print("\n Blue: ");
  Serial.print(blueSensorValue);

  // converts the 0-1024 reading from analogRead to the 8bit resolution 0-255
  redValue = redSensorValue/4;
  greenValue = greenSensorValue/4;
  blueValue = blueSensorValue/4;

  // reprints the new remapped values
  Serial.print("\n Mapped Sensor values \n Red: ");
  Serial.print(redValue);
  Serial.print("\n Green: ");
  Serial.print(greenValue);
  Serial.print("\n Blue: ");
  Serial.print(blueValue);

  // writes the new value to the RGB Pins
  analogWrite(redLEDPin, redValue);
  analogWrite(greenLEDPin, greenValue);
  analogWrite(blueLEDPin, blueValue);
  

}
