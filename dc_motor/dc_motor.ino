/*
Dara Klinkner
George Fox University
ENGR 152
3/16/2019

HW 8 Task 4
*/

// ******************************
//   Initialize Variables
// ******************************

const int switchPin = 2; // the number of the switching pin
const int motorPin =  9; // pin connected to the motor

int switchState = 0;  // switch's status

// ******************************
//   Setup Code
// ******************************

void setup() {
  
  pinMode(motorPin, OUTPUT); // motor pin is an output
  
  pinMode(switchPin, INPUT); // button pin is an input
}

// ******************************
//   Initialize Variables
// ******************************

void loop() {
  
  
  switchState = digitalRead(switchPin); // Is the switch on or off?

  if (switchState == HIGH) { // checks if switch is on

    digitalWrite(motorPin, HIGH); //turns the motor on
    
  } else {
    
    digitalWrite(motorPin, LOW); // motor is off 
  }
}
