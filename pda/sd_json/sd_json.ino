#include <ArduinoJson.h>
#include <SD.h>

const int sd_cs = 7;

struct Child {
  char *name;
  int age;
  int level;
  int xp;
  char *chores[2]; // -> points to character array in memory
};

void setup() {
  Serial.begin(9600);

  char *items[2] = { // -> points to array of arrays in memory
    "Take out the trash",
    "Wash the cat."
  };

  Child bobby;
  bobby.name = "Bobby";
  bobby.age = 13;
  bobby.level = 2;
  bobby.xp = 51;

  bobby.chores[0] = items[0];
  bobby.chores[1] = items[1];
  // Setup SD card

  pinMode(10, OUTPUT); // SPI always need pin 10 to be output.
  if(!SD.begin(sd_cs)) {
    while(1);
  }
  Serial.println(F("Successfully Initialized."));

  // -------------- Populate JSON -------------
  StaticJsonDocument<150> doc; // arduinojson.org/v6/assistant to compute capacity
  doc["name"] = bobby.name;
  doc["age"] = bobby.age;
  doc["level"] = bobby.level;
  doc["xp"] = bobby.xp;
  copyArray(bobby.chores, doc["chores"].to<JsonArray>());


  // ---------------------------- \/ ----------------------
  Serial.println(F("\n --------- Writing file... -------"));
  if (!SD.exists(F("/JSON/"))) {
    Serial.println(F("Directory doesn't exist"));
    SD.mkdir("JSON");
  }
  // Remove file first to start new.
  SD.remove(F("/JSON/a_file.txt"));
  // Check avialble memory first
  Serial.print(F("Available memory: ")); Serial.print(availableMemory());
  Serial.print(F("/2048 bytes\n"));
  // Open a file in the 'json' directory, in the write mode. Also creates the file again.
  File file = SD.open("/JSON/a_file.txt", FILE_WRITE);
  if (!file) { // error checking
    Serial.println(F("Failed to create file"));
    while(1);
  }
  serializeJsonPretty(doc, Serial); // send to serial first
  // Serialize JSON to file
  if (serializeJsonPretty(doc, file) == 0) {
    Serial.println(F("\nFailed to write to file"));
  }
  file.close();
  Serial.println(F("\nDone!"));


  // ------------------------- Reading file again -----------
  Serial.println("\n --------- Reading file... -------");
  // Open a file in the 'json' directory, in the read mode.
  File fileR = SD.open("/JSON/a_file.txt", FILE_READ);
  if(!fileR) {
    Serial.println(F("Failed to open file.")); // yes this does work.
    while(1);
  }
  while(fileR.available()) { // read from file until nothing else is left
    Serial.write(fileR.read());
  }
  StaticJsonDocument<150> doc2; // arduinojson.org/v6/assistant to compute capacity
  // Deserialize JSON from file.
  fileR.seek(0); // DO NOT forget to return to the beginning of the file.
  DeserializationError error = deserializeJson(doc2, fileR);
  if (error) {
    Serial.println(F("\nFailed to deserialize file.")); // yes this does work.
    Serial.print("Available memory: "); Serial.print(availableMemory());
    Serial.print("/2048 bytes\n");
    Serial.println(error.c_str());
  }

  JsonArray desChores = doc2["chores"].as<JsonArray>();
  Serial.print("\nChores: ");
  Serial.print("\n- Size: "); Serial.print(desChores.size());
  for(JsonVariant thing: desChores) {
    Serial.print("\n - ");
    Serial.print(thing.as<char*>());
  }

  fileR.close();
  Serial.println("\nDone!");


  // '&' Just means the adress of.
  // '*' means "I point to something"

  // an array is a pointer. An array points to the first element of the array.
  // char things[] is the same as doing `char *things`
  /*
  char things1[] = "hello world1";
  char *things2 = "hello world2"; // this works.
  Serial.println(things1[8]);
  Serial.println(things2[8]);
  */
}

void loop() {

}

int availableMemory() {
  int size = 2048; // Use 2048 with ATmega328
  byte *buf;
  while ((buf = (byte *) malloc(--size)) == NULL)
    ;
  free(buf);
  return size;
}
