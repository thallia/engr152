int availableMemory() {
  int size = 2048; // Use 2048 with ATmega328
  byte *buf;
  while ((buf = (byte *) malloc(--size)) == NULL)
    ;
  free(buf);
  return size;
}



  // These get filled later
  File current_child;
  StaticJsonDocument<240> json_obj;

  // open selected child
  current_child = SD.open("/JSON/childx.json");
  // Open into json doc
  DeserializationError error = deserializeJson(json_obj, current_child); // check error below please
  // (You can use the StaticJsonDocument to pull data from 'file' (memory) now)
  // How to access data now
  JsonArray desChores = json_obj["chores"].as<JsonArray>(); // access an array
  char *name = json_obj["name"]; // access string value
  int lvl = json_obj["lvl"]; // access integer value
