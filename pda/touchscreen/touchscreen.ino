
/*
Dara Klinkner, George Fox University
ENGR 152 Semester Project, P.D.A
Touchscreen Code
*/

// ************************
//  Include all libraries
// ************************

#include <SPI.h>
#include "Adafruit_GFX.h"
#include "Adafruit_RA8875.h"
#include <ArduinoJson.h>
#include <SD.h>


// *****************************************
//  Define RA8875 Pins and Program States
// *****************************************

// Library only supports hardware SPI at this time
// Connect SCLK to UNO Digital #13 (Hardware SPI clock)
// Connect MISO to UNO Digital #12 (Hardware SPI MISO)
// Connect MOSI to UNO Digital #11 (Hardware SPI MOSI)
#define RA8875_INT 3 // defines the interrupt pin
#define RA8875_CS 10 // touchscreen chip select
#define RA8875_RESET 9 // reset pin, 10k resistor
#define SD_CS 7

// define all the states!
#define HOMESCREEN 1 // first state, display the homescreen
#define CHORES 2 // display when they ask to display the chore list
#define NEWCHORE 3 // display when they ask to input a new chore for the kid
#define DAY 4 // when the kid picks a day, display the chores they're supposed to do on that day

// ************************
//  Variables
// ************************

// the starting state of the program
int currentState = HOMESCREEN;
boolean hasDisplayed = false; // so we don't blow up the serial monitor

uint16_t lastX = 0; // these are for touch debouncing
uint16_t lastY = 0;

// for displaying days of the week
char daysOfTheWeek[7][10] = {"Monday", "Tuesday", "Wednesday", "Thursday",
"Friday", "Saturday", "Sunday"};

//char choresToDo[9][15] = {"Wash dishes", "Walk dog", "Fold laundry", "Do homework",
//"Make bed", "Clean room", "Shower"};

// ----------------IMPORTANT READ--------------------//
/*
  When we get to the beginning of the program, we need to pull
  basic child data here. This includes first name, last name, level,
  experience, and potentially mon-sun chores. We can also have the
  chores pulled when the touch functions are activated.
*/

// for now, all the strings are hardcoded for testing purposes
// prep all the data to be printed
// first name needs to be 9 char max
//char firstName[] = "Bobby";
// last name needs to be
//char lastName[] = "Bargfield";
// Level
char level[] = "Level: ";
// level Level
//char actualLevel[] = "3";
// experience number, that hopefully changes ;; 1800 max
//char xP[] = "1000";
// XP prefix
char eXPerience[] = "XP:  ";
// day to display question
char dayDisplay[] = "What day would you like to view?";
// back button
char backButton[] = "Back";

char *firstName;
char *lastName;
int lvl;
int xp;

// int variables we need to pull from data
//int xpValue = 1000; // initial XP value we should get from data
// map(value, fromLow, fromHigh, toLow, toHigh)
//int xpDisplay = map(xp, 0, 1800, 20, 140); // mapped to pixels so we don't overwhelm the screen

// was the day button triggered?
boolean dayButtonWork = true;
boolean touched = false;
boolean trial;
boolean keepGoing = true;

int dayChosen; // so we know what day we gets
boolean breakDisplayed = false;

// **********************************
//   Init RA8875 Class& Touch Inputs
// **********************************
Adafruit_RA8875 tft = Adafruit_RA8875(RA8875_CS, RA8875_RESET); // assigns the ra8875 call to tft so we don't have to type it all out every time
uint16_t tx, ty; // touch inputs

char levelArray[3];
char xpArray[5];
JsonArray desChores;

int xpDisplay;
int cursor_x = 185;
int cursor_y = 120;

boolean completedChore_1 = false;
boolean completedChore_2 = false;
boolean completedChore_3 = false;
boolean completedChore_4 = false;
boolean completedChore_5 = false;
boolean completedChore_6 = false;

// ************************
//  Setup Code
// ************************

void setup()
{
  Serial.begin(9600); // Begin the serial monitor!

  if(!SD.begin(SD_CS)) {
    while(1);
  }
  Serial.println(F("\n SD Card successfully Initialized."));
  Serial.println(availableMemory());

  if(SD.exists("/json/")){
    Serial.print(F("this worked\n"));
    //Serial.print(availableMemory());
  }

  File current_child;
  StaticJsonDocument<138> json_obj;

  Serial.print(F("------reading for files...-----\n"));
  current_child = SD.open("/json/child1.txt", FILE_READ);
  if(!current_child){
    Serial.println(F("ERRFILE"));
    while(1);
  }
  Serial.print(availableMemory());

// open selected child
  //current_child = SD.open("/json/child-1.json");

  // display the file data
   /*
  while(current_child.available()){
    Serial.write(current_child.read());
  }

// Open into json doc
  current_child.seek(0); */

  DeserializationError error = deserializeJson(json_obj, current_child); // check error below please

//  if(error){
  //  Serial.println(error.c_str());
    //while(1);
  //}
  // (You can use the StaticJsonDocument to pull data from 'file' (memory) now)
  // How to access data now
  desChores = json_obj["chores"].as<JsonArray>(); // access an array

  // get all the data
  firstName = json_obj["name"]; // access string value
  lastName = json_obj["lastname"];
  lvl = json_obj["level"]; // access integer value
  xp = json_obj["xp"];

  Serial.println("\nLevel: " + String(lvl));
  Serial.println("xp: " + String(xp));

  String(lvl).toCharArray(levelArray, 3);
  String(xp).toCharArray(xpArray, 5);
// int variables we need to pull from data
//int xpValue = 1000; // initial XP value we should get from data
// map(value, fromLow, fromHigh, toLow, toHigh)
xpDisplay = map(xp, 0, 1800, 20, 140); // mapped to pixels so we don't overwhelm the screen

  Serial.println(F("RA8875 start")); // display that the RA8875 is ON

  /* Initialise the display using 'RA8875_480x272' or 'RA8875_800x480' */
  if (!tft.begin(RA8875_800x480)) { // make it 800x480 because that's the size of our touchscreen
    Serial.println(F("RA8875 Not Found!")); // if the initialization fails -- typically incorrect wiring
    while (1); // won't print until it initializes correct
  }

  Serial.println(F("Found RA8875")); // initialization success

  tft.displayOn(true); // turn display on
  tft.GPIOX(true);      //
  tft.PWM1config(true, RA8875_PWM_CLK_DIV1024); // PWM output for backlight
  tft.PWM1out(255);

  // this will be the start of the homeScreen function
  tft.fillScreen(RA8875_BLACK); // fills screen with blackness of death
  current_child.close();
}

// ************************
//  Main Loop
// ************************

void loop () {

  switch (currentState) { // currentState is going to be displaying the homescreen

    case HOMESCREEN:
      // display the homescreen
      if(!breakDisplayed){
        homeScreen();
      } else {
        break;
      }

    case CHORES: // next state will be if they press the chore display button
      // display the child data and ask what day of chores they want to see

    if(!breakDisplayed){
        choreList();
      } else {
        break;
      }

    case DAY:
      // this is going to, based on the day picked, display the chores due that day
      if(!breakDisplayed){
        displayDayChores();
      } else {
        break;
      }

    // if i actually had time to complete this and space wasn't an issue, this is where the NEWCHORE
    // function would go.
    //case NEWCHORE:

  }
}

// ************************
//  User Defined Functions
// ************************

// CHECK TOUCH FUNCTION
void checkTouch() { // see if the screen has been touched, and if so, where?
  while ((tx == 0) && (ty == 0)){
    if (tft.touched() == true) {

        tft.touchRead(&tx, &ty); // gets x and y coordinates of where it was touched
        Serial.print("X: " + String(tx)); // prints coordinates for debugging
        Serial.println(", Y: " + String(ty));

    }
  }
}

void writeText(int pixDistance, const char* labelName, const char* numBer){
  tft.textSetCursor(20, pixDistance); // provide the distance needed
  tft.textWrite(labelName); // the thing you're gonna write that prefixes a number
  tft.textWrite(numBer); // the number that comes after
  // you'll get something like "Level: 30"
}

void writeDay(int day, int textCursorX, int textCursorY, int xCoord, int yCoord){
    // we're gonna draw the rectangles
    tft.fillRect(xCoord, yCoord, 175, 75, RA8875_BLUE);

    tft.textTransparent(RA8875_WHITE);
    tft.textSetCursor(textCursorX, textCursorY);
    tft.textWrite(daysOfTheWeek[day]);
    //tft.textSetCursor(215, 150);
    //tft.textWrite(daysOfTheWeek[0]);

}

void writeDayChosen(){

  hasDisplayed = true;
  while(hasDisplayed){
      Serial.print(F("day button worked\n")); // prints if a button was pressed for debugging
      hasDisplayed = false; // so we don't PWM the actual display text
      Serial.print("Day: " + String(dayChosen) + "\n"); // debugging statement
      dayButtonWork = false; // did the button print? if so, break out of the while loop
      breakDisplayed = false;
      currentState = DAY; // go to the next state
      trial = false;

  }
}

void outlineAndProfile(){
  // outline
  tft.drawRect(10, 10, 780, 460, RA8875_BLUE);
  tft.drawRect(10, 10, 165, 460, RA8875_BLUE); // draw the rectangle for the kid's profile
  tft.drawRect(10, 10, 165, 80, RA8875_BLUE);
  tft.fillRect(10, 90, 780, 25, RA8875_BLUE); // break bar
  tft.drawRect(20, 190, 140, 15, RA8875_GREEN); // xp bar outline
  tft.drawRect(20, 850, 145, 60, RA8875_WHITE); // back button

  // this was cool but i don't need it but i'm so impressed i figured it out i don't wanna delete it
  //int currentWidth = 165; // the current place there's a rectangle
  //for(int x = 5; x > 1; x--){
    //  currentWidth = currentWidth + 125; // add 125 pixels to each rectangle
      //tft.drawRect(10, 10, currentWidth, 460, RA8875_BLUE); // draw the rectangle for each day
  //}

  // display all the text!
  tft.textMode();
  tft.textTransparent(RA8875_WHITE); // make the text the color that we want it to be
  tft.textEnlarge(1); // for some reason this is needed so we don't get weird symbols

  // where we will display the first name
  tft.textSetCursor(20, 20); // set where the text will start
  tft.textWrite(firstName); // write the text

  // last lastName
  tft.textSetCursor(20, 50); // where the text will start
  tft.textWrite(lastName); // write the text

  // back button
  tft.textSetCursor(60, 860);
  tft.textWrite(backButton);

  // Level prefix + number
  writeText(120, level, levelArray); // this function prevents code bloat, it does what's commented out below
  //tft.textSetCursor(20, 120);
  //tft.textWrite(level);
  //tft.textWrite(lvl);

  // experience
  String(xp).toCharArray(xpArray, 5);
  writeText(150, eXPerience, xpArray);
  //tft.textSetCursor(20, 150);
  //tft.textWrite(eXPerience);
  //tft.textWrite(xP);

  // map function for xp bar
  // map(value, fromLow, fromHigh, toLow, toHigh)
  tft.fillRect(20, 190, xpDisplay, 15, RA8875_GREEN); // draw the XP bar

}

void choreRectDisplay(){
  int dist = 85;

  int sq_y = 130;

  tft.drawRect(175, 90, 310, 380, RA8875_BLUE);
  for (int w = 5; w > 0; w--){
    tft.drawRect(175, 90, 310, dist, RA8875_BLUE);
    dist = dist + 60;
  }

  for (int w = 5; w >= 0; w--){
    tft.drawRect(620, sq_y, 40, 40, RA8875_GREEN);
    sq_y = sq_y + 58;
  }

  /*int dist = 205;

  for (int w = 2; w > 0; w--){

    tft.drawRect(175, 90, dist, 380, RA8875_BLUE);
    dist = dist + 205;
    //tft.drawRect(175, 90, 410, 380, RA8875_BLUE);
    //tft.drawRect(175, 90, 615, 380, RA8875_BLUE);
  }
  int yDown = 115;
  for (int y = 2; y >= 0; y--){
    tft.drawRect(175, yDown, 205, 118, RA8875_BLUE);
    yDown = yDown + 118;

  }
  yDown = 115;
  for (int y = 2; y >= 0; y--){
    tft.drawRect(380, yDown, 205, 118, RA8875_BLUE);
    yDown = yDown + 118;

  }

  yDown = 115;
  for (int y = 2; y >= 0; y--){
    tft.drawRect(585, yDown, 205, 118, RA8875_BLUE);
    yDown = yDown + 118;

  }*/



}

// *************************************
//    HOME SCREEN DISPLAY
// *************************************
void homeScreen()
{

  tft.fillScreen(RA8875_BLACK);
  //           x   y   width height  color
  tft.drawRect(10, 10, 780, 460, RA8875_BLUE);
  tft.fillRect(50, 150, 300, 200, RA8875_BLUE); // left button
  tft.fillRect(450, 150, 300, 200, RA8875_MAGENTA); // right button
  tft.drawRect(10, 10, 165, 80, RA8875_BLUE);
  tft.drawRect(10, 10, 780, 80, RA8875_BLUE);


  // first button text
  tft.textMode(); // make sure we're in text mode or it won't display
  char firstButton[] = "Chores "; // initialize what we want to display in an array
  tft.textSetCursor(125, 215); // x & y where the text will start
  tft.textTransparent(RA8875_WHITE); // choose the text color
  tft.textEnlarge(2); // make sure this is the right size or it won't print correctly
  tft.textWrite(firstButton); // write what we stored in the array

  // second button text
  char secondButton[] = "New Chore "; // store what we want to display in the second array
  tft.textSetCursor(495, 215); // set the x and y coordinates for the second secondButton
  tft.textTransparent(RA8875_WHITE); // choose the text color
  tft.textEnlarge(2); // size the text
  tft.textWrite(secondButton); // write the text

  // Kid's profile
  tft.textEnlarge(1); // for some reason this is needed so we don't get weird symbols
  // where we will display the first name
  tft.textSetCursor(20, 20); // set where the text will start
  tft.textWrite(firstName); // write the text

  // last lastName
  tft.textSetCursor(20, 50); // where the text will start
  tft.textWrite(lastName); // write the text

  // Level prefix + number
  tft.textSetCursor(190, 35);
  tft.textWrite(level);
  tft.textWrite(levelArray);

  // experience
  tft.textSetCursor(500, 20);
  tft.textWrite(eXPerience);
  tft.textWrite(xpArray);

  // xp
  tft.drawRect(500, 60, 140, 15, RA8875_GREEN); // xp bar outline
  tft.fillRect(500, 60, xpDisplay, 15, RA8875_GREEN); // draw the XP bar

  hasDisplayed = true; // make this true so we don't print a million things to the serial monitor
  breakDisplayed = true;

  tft.touchEnable(true); // enables touchscreen

  while(hasDisplayed == true){ // while we have the homescreen displayed, do this stuff
        tx = 0;
        ty = 0;
        checkTouch(); // reads previous touch thingy
        tx = 0;
        ty = 0;
        checkTouch();
    if (((tx >= 50) && (tx <= 300)) && (ty >= 215)) { // if touched within the first button bounds...

        Serial.print(F("this worked\n")); // debugging statement so we know it worked
        hasDisplayed = false; // make this false for the next state
        breakDisplayed = false;
        currentState = CHORES; // go to display the chores so we can obey the button press

      }
    }
}
// ********************************
// CHORE LIST FUNCTION
// ********************************
void choreList() {

    Serial.println("Current State: " + String(currentState));
    delay(500);
    //tft.touchEnable(false);
    //tft.touchEnable(true);
    tft.fillScreen(RA8875_BLACK); // clear the screen
    outlineAndProfile();

    // day to display
    tft.textSetCursor(240, 50);
    tft.textTransparent(RA8875_WHITE);
    tft.textWrite(dayDisplay);

    // make text the size we need it to be for the buttons
    tft.textEnlarge(1);

    // monday
    writeDay(0, 215, 160, 200, 140); // args: day, text X, text Y, rectangle x & y
    // Tuesday
    writeDay(1, 215, 250, 200, 230);
    // Wednesday
    writeDay(2, 215, 340, 200, 320);
    // Thursday
    writeDay(3, 415, 160, 400, 140);
    // Friday
    writeDay(4, 415, 250, 400, 230);
    // Saturday
    writeDay(5, 415, 340, 400, 320);
    // Sunday
    writeDay(6, 615, 250, 600, 230);

    // check for which day button has been pressed!
    hasDisplayed = true;
    breakDisplayed = true;
    dayButtonWork = true;

      // these will test if any of the buttons have been triggered
      while(dayButtonWork) {
        //Serial.print("are we getting here");
        tx = 0;
        ty = 0;
        checkTouch(); // reads previous touch thingy
        tx = 0;
        ty = 0;
        checkTouch();
        Serial.print("new tx: " + String(tx)); // prints them for debugging purposes
        Serial.print(", new ty: " + String(ty) + "\n");
        if (tx != 0 && ty !=0){ // if the new touches aren't at 0 coordinates, move on!
          trial = true;
        }

        // this was once useful code but it sucked so i made something better
        /*while(hasDisplayed == true){
          if (tft.touched()){
            tft.touchRead(&tx, &ty);
          }


          while(!tft.touched() == keepGoing){

            for (int x = 0; x <= 150; x++){
              Serial.print("tx: " + String(tx));
              Serial.print(", ty: " + String(ty) + "\n");
              if (((oldTx <= tx) || (oldTx >= tx)) || ((oldTy <= ty) || (oldTy >= ty))) {
                tft.touchRead(&tx, &ty);
                trial = true;
              }
            }
            keepGoing = false;
          }*/

          while(trial){
            checkTouch();
            tx = 0;
            ty = 0;
            checkTouch();

            if ((tx >= 20) && (tx <= 165) && (ty >= 700)) {
              // we chose the back button here!
              currentState = HOMESCREEN;
              Serial.print("chose to go back to homescreen");
              Serial.print("3rd tx: " + String(tx));
              Serial.println(", 3rd ty: " + String (ty));
              trial = false;
              dayButtonWork = false;
              tx = 0;
              ty = 0;
              breakDisplayed = false;

            } else if ((tx >= 250) && (tx <= 500) && (ty >= 360) && (ty <= 515)){

              dayChosen = 0;
              writeDayChosen();

            } else if ((tx >= 250) && (tx <= 500) && (ty >= 515) && (ty <= 675)) {

              dayChosen = 1;
              writeDayChosen();

            } else if ((tx >= 250) && (tx <= 500) && (ty >= 675)) {

              dayChosen = 2;
              writeDayChosen();

            } else if ((tx >= 500) && (tx <= 730) && (ty >= 360) && (ty <= 515)) {

              dayChosen = 3;
              writeDayChosen();

            } else if ((tx >= 500) && (tx <= 730) && (ty >= 515) && (ty <= 675)) {

              dayChosen = 4;
              writeDayChosen();

            } else if ((tx >= 500) && (tx <= 730) && (ty >= 675)) {

              dayChosen = 5;
              writeDayChosen();

            } else if ((tx >= 730) && (ty >= 515)) {

              dayChosen = 6;
              writeDayChosen();
            }

          }

      }

    //}

  }
void displayTextChores(const char* choreForThatDay, int x, int y){

  tft.textMode();
  tft.textSetCursor(cursor_x, cursor_y);
  tft.textEnlarge(1);
  tft.textTransparent(RA8875_WHITE);
  tft.textWrite(choreForThatDay);
}

// ****************************
//    DISPLAY CHORES
// ****************************
void displayDayChores(){
  /*
     this is where we'll have it determine whether or not the kind
     chose Monday/Tuesday/Wednesday/Thurs/Fri/Sat/Sun to display the
     chores, and then it'll get the JSON data and display the chores
     for that day in particular so we don't have a bunch of repeating
     code.
  */

  Serial.println(F("\nWe're into display chores!"));

  tft.fillScreen(RA8875_BLACK); // clears the screen

  outlineAndProfile(); // repetitive drawing code

  // displays day chosen
  tft.textMode();
  tft.textSetCursor(400, 25);
  tft.textEnlarge(2);
  tft.textTransparent(RA8875_WHITE);
  tft.textWrite(daysOfTheWeek[dayChosen]);

  choreRectDisplay();
  cursor_y = 130;
  for(JsonVariant v : desChores) {

    displayTextChores(v.as<const char*>(), cursor_x, cursor_y);
    Serial.println(v.as<const char*>());

    cursor_y = cursor_y + 60;

  }

  //for (int i = 6; i >= 0; i--){
    //Serial.print("Day chosen: " + String(dayChosen));
    //if (dayChosen == i){
      //displayTextChores(i, 205, 90);
    //}
  //}
  if (completedChore_1){

    tft.fillRect(620, 130, 40, 40, RA8875_GREEN);

  }
 if (completedChore_2){

      tft.fillRect(620, 188, 40, 40, RA8875_GREEN);
    }


  hasDisplayed = true;
  while (hasDisplayed){
    tx = 0;
    ty = 0;
    checkTouch(); // reads previous touch thingy
    tx = 0;
    ty = 0;
    checkTouch();

    if(((tx <= 790) && (tx >= 775)) && ((ty <= 370) && (ty >= 350))){

        tft.fillRect(620, 130, 40, 40, RA8875_GREEN);
        xp = xp + 20;
        completedChore_1 = true;

    } else if ((tx <= 790) && (tx >= 775) && (ty <= 470) && (ty >= 450)){

        tft.fillRect(620, 188, 40, 40, RA8875_GREEN);
        xp = xp + 20;
        completedChore_2 = true;

    }


    if ((tx >= 20) && (tx <= 165) && (ty >= 700)) {
    // we chose the back button here!
      currentState = CHORES;
      Serial.print(F("chose to go back to chores"));
      Serial.print("3rd tx: " + String(tx));
      Serial.println(", 3rd ty: " + String (ty));
      trial = false;
      dayButtonWork = false;
      hasDisplayed = false;
      breakDisplayed = false;
      tx = 0;
      ty = 0;
      //completedChore = false;

    }
 }



}

int availableMemory() {
  int size = 2048; // Use 2048 with ATmega328
  byte *buf;
  while ((buf = (byte *) malloc(--size)) == NULL);
  free(buf);
  return size;
}
