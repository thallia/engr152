/*
 * Dara Klinkner, George Fox University
 * ENGR 152
 * HW 7 Task 3
 * 3/8/2018
 * 
 * A small game to play Rock, Paper, Scissors with.
 * 
 */

// ****************************************
//   Variables 
// ****************************************

// define variables first
int currentState = 1; // sets current state of the program to 1
int playerOneChoice; // where we'll store the choices of each player
int playerTwoChoice; 
int computerChoice;
int mode = 0; // for if we chose 1 or 2 player

// this is so the serial.print statements don't run 50 times before input from the player
boolean didRead = false;
boolean didRead2 = false;

// then define constants
const int rockPin = 2;      // defines the three button inputs
const int paperPin = 3;
const int scissorPin = 4; 

// defines these to make reading the code easier
const int rock = 1;
const int paper = 2;
const int scissors = 3;

// define the states
const int welcomeState = 1;
const int rulesState = 2;
const int playerSelect = 3;
const int onePlayer = 4;
const int twoPlayer = 5;
const int gameResults = 6;
const int playAgain = 7;



// ****************************************
//   Setup Code
// ****************************************
void setup() {

  Serial.begin(9600); // start the serial monitor
  
  pinMode(rockPin, INPUT);   // defines the buttons connected to the pins as inputs
  pinMode(paperPin, INPUT);
  pinMode(scissorPin, INPUT);

}


// ****************************************
//    Main Code
// ****************************************
void loop() {
  // put your main code here, to run repeatedly:

  switch (currentState) {
	
	case welcomeState:
	
	  if (didRead == false) { // checks if printed yet, if not, print!
		  
      Serial.print("Welcome to Rock, Paper, Scissors! \n"); // prints welcome statement and adds a newline
		  Serial.println("Press any button to begin. \n \n");      // tells the user to press a button to start

		  didRead = true; // tells us we printed the thing so we don't do it again
	  } 

    
	
	  if ((digitalRead(rockPin)) || (digitalRead(paperPin)) || (digitalRead(scissorPin)) == true) { // if any of the pins read high, change the state
      delay(250); // prevent switch debounce
		  currentState = rulesState; // change the state to 2

		  didRead = false; // prepare for the next printed statement
	  }
	
	  break; // break out of case 1

	case rulesState:

	  if (didRead == false) {	
		  // print the rules
		  Serial.print("\n Rules: \n"); 
		  Serial.print("1) Rock crushes scissors \n2) Scissors cuts paper \n3) Paper covers rock \n");
		  Serial.println("Press any button to play."); 
		
		  didRead = true; // we already printed things
	  }

	
	  if ((digitalRead(rockPin)) || (digitalRead(paperPin)) || (digitalRead(scissorPin)) == true) { // if any of the pins read HIGH, change the state
      delay(250); // prevent switch debounce
		  currentState = playerSelect; // change the state to 3
		  didRead = false; // prepare for the next round of printing
	  }

	  break; // break out of case 2
	
	case playerSelect:
	
	  if (didRead == false) {
	    Serial.print("\n Select one or two players. \n"); // asks player to select the computer opponent or if they're playing with a friend
      Serial.print("If 1 player, press the middle button. If 2 players, press the right button. \n \n");
		  didRead = true; // we already printed things!
	  }

	  if ((digitalRead(paperPin)) == HIGH) { // if the middle button is pressed, switch to the one Player state.
      
      delay(250); // switch debounce
		  currentState = onePlayer; // changes state to one player option
	    mode = 1;
	    didRead = false; // resets for next printing round

	  } else if ((digitalRead(scissorPin)) == HIGH) {

    
      delay(250); // switch debounce
	    currentState = twoPlayer; // changes state to two player option	
	    mode = 2;
	    didRead = false; // resets for next printing round
	
	  }

	  break; // break out of case 3

	case onePlayer: // one player state

	  if (didRead == false) {
		  Serial.print("Please choose an attack: \n 1) Rock \n 2) Paper \n 3) Scissors \n");
		  didRead = true; // we printed the message already!
	  }
	
	  if ((digitalRead(rockPin)) == HIGH) { // if they press the 1st button, Rock
      delay(250); // switch debounce
		  playerOneChoice = rock; // 1
		  didRead = false; // resets for next printing round
      currentState = gameResults; // sets next state to finish game

	  } else if ((digitalRead(paperPin)) == HIGH) { // if they press the 2nd button, paper
      delay(250); // switch debounce
		  playerOneChoice = paper; // 2
		  didRead = false; // resets for next printing round
      currentState = gameResults; // sets next state to finish game

	  } else if ((digitalRead(scissorPin)) == HIGH) { // if they press the 3rd button, scissors
      delay(250); // switch debounce
		  playerOneChoice = scissors; // 3
		  didRead = false; // resets for next printing round
      currentState = gameResults; // sets next state to finish game

	  }

	 computerChoice = random(1,3);

	  break;

	case twoPlayer:

	  if (didRead == false) {

		  Serial.print("Player One, please choose an attack: \n 1) Rock \n 2) Paper \n 3) Scissors \n");
		  didRead = true;
	  }
	
	  if ((digitalRead(rockPin)) == HIGH) { // if they press the 1st button, Rock
    
      delay(250); // switch debounce
		  playerOneChoice = rock; // 1
		  didRead = false; // resets for next printing round

	  } else if ((digitalRead(paperPin)) == true) { // if they press the 2nd button, paper
      
      delay(250); // switch debounce
		  playerOneChoice = paper; // 2
		  didRead = false; // resets for next printing round

	  } else if ((digitalRead(scissorPin)) == true) { // if they press the 3rd button, scissors
    
      delay(250); // switch debounce
		  playerOneChoice = scissors; // 3
		  didRead = false; // resets for next printing round

	  }
	
	  if (didRead == false) {
		  Serial.print("Player Two, please choose an attack: \n 1) Rock \n 2) Paper \n 3) Scissors \n");
		  didRead = true;
	  }
	

	  if ((digitalRead(rockPin)) == HIGH) { // if they press the 1st button, Rock
    
      delay(250); // switch debounce
		  playerTwoChoice = rock; // 1
		  didRead = false; // resets for next printing round
      currentState = gameResults; // sets next state to finish game
      break;

	  } else if ((digitalRead(paperPin)) == true) { // if they press the 2nd button, paper
      
      delay(250); // switch debounce
		  playerTwoChoice = paper; // 2
		  didRead = false; // resets for next printing round
      currentState = gameResults; // sets next state to finish game
      break;

	  } else if ((digitalRead(scissorPin)) == true) { // if they press the 3rd button, scissors
    
      delay(250); // switch debounce
		  playerTwoChoice = scissors; // 3
		  didRead = false; // resets for next printing round
      currentState = gameResults; // sets next state to finish game
      break;

	  }

   break;
	
	case gameResults:
	
	  if (mode == 1) { // if it's one player versus the computer

		  if (playerOneChoice == computerChoice){ // if they tie

        Serial.print("\n The computer chose: ");
        Serial.println(computerChoice);
			  Serial.print("\n It's a tie!");

		  } else if (playerOneChoice == rock && computerChoice == paper) { // if player chooses rock and computer chooses paper
			
			  Serial.print("\n Paper beats rock!");
			  Serial.print("\n Computer wins.");
					
		  } else if (playerOneChoice == rock && computerChoice == scissors) { // if player chooses rock and computer chooses scissors
			
			  Serial.print("\n Rock beats scissors!");
			  Serial.print("\n You win!");	

		  } else if (playerOneChoice == paper && computerChoice == rock) { // if player chooses paper and computer chooses rock
			
			  Serial.print("\n Paper beats rock!");
			  Serial.print("\n You win!");	

		  } else if (playerOneChoice == paper && computerChoice == scissors) { // if player chooses paper and computer chooses scissors

			  Serial.print("\n Scissors beats paper!");
			  Serial.print("\n Computer wins.");

		  } else if (playerOneChoice == scissors && computerChoice == rock) { // if player chooses scissors and computer chooses rock
			
			  Serial.print("\n Rock beats scissors!");
			  Serial.print("\n Computer wins.");

		  } else if (playerOneChoice == scissors && computerChoice == paper) { // if player chooses scissors and computer chooses paper
			
			  Serial.print("\n Scissors beats paper!");
			  Serial.print("\n You win!");

		  }

	  } else {

		  if (playerOneChoice == playerTwoChoice){ // if they tie

			  Serial.print("It's a tie!");

		  } else if (playerOneChoice == rock && playerTwoChoice == paper) { // if player chooses rock and player 2 chooses paper
			
			  Serial.print("\n Paper beats rock!");
			  Serial.print("\n Player Two wins.");
					
		  } else if (playerOneChoice == rock && playerTwoChoice == scissors) { // if player chooses rock and player 2 chooses scissors
			
			  Serial.print("\n Rock beats scissors!");
			  Serial.print("\n You win!");	

		  } else if (playerOneChoice == paper && playerTwoChoice == rock) { // if player chooses paper and player 2 chooses rock
			
			  Serial.print("\n Paper beats rock!");
			  Serial.print("\n You win!");	

		  } else if (playerOneChoice == paper && playerTwoChoice == scissors) { // if player chooses paper and player 2 chooses scissors

			  Serial.print("\n Scissors beats paper!");
			  Serial.print("\n Player Two wins.");

		  } else if (playerOneChoice == scissors && playerTwoChoice == rock) { // if player chooses scissors and player 2 chooses rock
			
			  Serial.print("\n Rock beats scissors!");
			  Serial.print("\n Player Two wins.");

		  } else if (playerOneChoice == scissors && computerChoice == paper) { // if player chooses scissors and player 2 chooses paper
			
			  Serial.print("\n Scissors beats paper!");
			  Serial.print("\n You win!");

		  }

	  }
	
	  currentState = playAgain; // change the state to Play Again
	

	case playAgain:

	  if (didRead == false) {

		  Serial.print("\n Do you want to play again? \n Press the middle button for yes and any other button for no.");
		  didRead = true; // we already printed!
	  }
	
	  if (digitalRead(paperPin) == HIGH) {
		  delay(250); // switch debounce
		  currentState = playerSelect; // if play again, send back to choosing the amount of players
	
	  } else {
		
		  currentState = welcomeState; // otherwise, reset the game
	  }


  }


}
