/*
 * Dara Klinkner
 * George Fox University
 * ENGR 152
 * 
 * A small program to act like a piano keyboard.
 * 
*/


// *****************************
//   Initialize Arrays & Vars
// *****************************

int notes[] = {262, 294, 330, 349}; // creates an array with integers representing frequency notes C, D, E, and F


// *****************************
//   Setup Code
// *****************************

void setup() {
  
  Serial.begin(9600); // start serial monitor
  
}


// *****************************
//   Main Code
// *****************************

void loop() {
  
  int keyVal = analogRead(A0); // local variable to hold the A0 input
  
  Serial.println(keyVal); // prints the input

  // play the note corresponding to each value on A0
  
  if (keyVal <= 1023 && keyVal >= 1020) { // if the analog values are between these two, play the resulting sound
    
    tone(8, notes[0]); // play C
    
  } else if (keyVal >= 990 && keyVal <= 1010) {
    
    tone(8, notes[1]); // play D
    
  } else if (keyVal >= 505 && keyVal <= 515) {
    
    tone(8, notes[2]); // play E
    
  } else if (keyVal >= 5 && keyVal <= 10) {
    
    tone(8, notes[3]); // play F
    
  } else {

    noTone(8);
    
  }
}
