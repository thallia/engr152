
/*
 * Dara Klinkner, George Fox University
 * ENGR 152
 * HW 7 Task 2
 * 3/8/2019
 * 
 */

// ********************************************************
//      Variables & Libriaries
// ********************************************************

const int STATE_ONE = 1;
const int STATE_TWO = 2;
const int STATE_THREE = 3;

int currentState = 1;
int currentTime = 0;
int previousTime = 0;
int previousTime_c3 = 0;
int interval = 1000;
int second_interval = 3000;

// ********************************************************
//      Setup Code
// ********************************************************
void setup() {

  Serial.begin(9600); // starts the serial monitor
  
}


// ********************************************************
//       Main Loop
// ********************************************************
void loop() {

  switch(currentState) {

    case STATE_ONE: // idle

    Serial.print("Welcome to the sample state machine. \n"); // prints basic message

    currentState = STATE_TWO; // changes state
    break; // leaves state

    case STATE_TWO: // if currentState evalues to 2
    
    currentTime = millis(); // gets current time from the millis function
    
    if ( currentTime - previousTime > interval) { // makes sure to wait 1 second before displaying message

      previousTime = currentTime; // sets counter again

      Serial.print("Some fruit flies are genetically resistant to getting drunk — but only if they have an inactive version of a gene scientists have named 'happyhour'.\n"); // prints message
    }

    currentState = STATE_THREE; // sets the new state to jump to

    break; // leaves state

    case STATE_THREE: // if currentState evaluates to 3

      if (currentTime - previousTime_c3 > second_interval) { // makes sure to wait 3 seconds before displaying message, without messing up case 2's previousTime

      previousTime_c3 = currentTime; // sets counter again

      Serial.print("On an average day, a typist's fingers travel 12.6 miles.\n"); // prints message
    }

    currentState = STATE_ONE;

    break;
    
  }

}
