/*
 * Dara Klinkner, George Fox University
 * ENGR 152
 * HW #6 Task #4
 * 3/2/2019
 * 
 */

// ******************************************
//    Libraries and Variable Declarations
// ******************************************
#include <Servo.h> // includes the servo library

Servo myServo; // initializes Servo object

int const potPin = A0; // analog pin connected to potentiometer
int potVal;
int angle;


// ******************************************
//    Setup Code
// ******************************************
 
void setup() {

  myServo.attach(7); // attaches object to recognize the motor on pin 9

  Serial.begin(9600); // starts the serial monitor

}


// ******************************************
//    Main Loop
// ******************************************

void loop() {

  potVal = analogRead(potPin); // reads the potentiometer value from the analog pin
  Serial.print("potVal: ");
  Serial.print(potVal); // prints value of potentiometer

  angle = map(potVal, 0, 1023, 0, 179); // scales the analog input to a voltage value
  Serial.print(", angle: "); 
  Serial.print(angle); // prints current angle

  myServo.write(angle); // tells the servo motor to move
  delay(15); // kills the microcontroller for 15ms

  for(int x = 0; x <= 180; x++) {
    myServo.write(x);
    delay(15);
  }

  for(int x = 180; x >= 0; x--) {
    myServo.write(x);
    delay(15);
  }

}
