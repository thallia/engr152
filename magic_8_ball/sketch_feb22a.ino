/*
 * Dara Klinkner
 * ENGR 152, George Fox University
 * HW #5 Task #2
*/

// ====================================================
//        Libraries & Variables
// ====================================================

#include <LiquidCrystal.h> // includes library for the display we're using
LiquidCrystal lcd(12, 11, 5, 4, 3, 2); // tells the Arduino what pins are used to communicate with the LCD screen

const int switchPin = 6; // the pin to be switched
int switchState = 0; // the state of the pin
int prevSwitchState = 0; // the previous state of the pin
int reply;


// ====================================================
//        Setup Code
// ====================================================
void setup() {
  
  lcd.begin(16, 2); // turns the LCD screen on
  pinMode(switchPin, INPUT); // defines pin 6 as an input
  lcd.print("Ask the"); // prints on the first line
  lcd.setCursor(0, 1); // moves to the second line of display
  lcd.print("Crystal Ball!"); // prints on the second line
  
}


// ====================================================
//        Main Code
// ====================================================
void loop() {

  switchState = digitalRead(switchPin); // checks the state and assigns it to the variable

  if (switchState != prevSwitchState) { // checks if switchState is the same as before
    if (switchState == LOW) {
      reply = random(8); // chooses a random number from 0-8
      lcd.clear();       // clears the screen
      lcd.setCursor(0, 0); // sets the line to the top display
      lcd.print("The ball says:");
      lcd.setCursor(0, 1); // moves to second line
      switch(reply) { // finds number and executes associated case
        
        case 0: 
        lcd.print("Yes");
        break;
        
        case 1:
        lcd.print("Most likely");
        break;
        
        case 2:
        lcd.print("Certainly");
        break;
  
        case 3:
        lcd.print("Outlook good");
        break;

        case 4:
        lcd.print("Unsure");
        break;

        case 5:
        lcd.print("Ask again");
        break;

        case 6:
        lcd.print("Doubtful");
        break;

        case 7:
        lcd.print("No");
        break;
      }
    }
  }
  prevSwitchState = switchState; // assigns last state so there isn't repeat

}
