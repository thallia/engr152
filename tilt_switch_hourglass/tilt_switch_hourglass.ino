/*
 * Dara Klinkner, George Fox University
 * ENGR 152
 * HW 6, Task 5
 * 3/2/19
 * 
 */


// ****************************************
//    Variable Initialization
// ****************************************

const int switchPin = 8;

unsigned long previousTime = 0; // holds the time the LED was last changed

int switchState = 0;
int prevSwitchState = 0;

int led = 2; // first LED pin

long interval = 600; // delay between lights


// ****************************************
//     Pin Setup
// ****************************************
void setup() {
  
  for(int x = 2; x < 8; x++) {
    pinMode(x, OUTPUT); // initializes LED pins as outputs
  }

  pinMode(switchPin, INPUT); // initializes the tilt switch pin as input
}


// ****************************************
//     Main loop
// ****************************************
void loop() {

  unsigned long currentTime = millis(); // gets the current time since the sketch started from the millis function

  if(currentTime - previousTime > interval ) { // checks if difference in time is greater than the interval -- if so, light the next LED and start counting again
    previousTime = currentTime;

    digitalWrite(led, HIGH);
    led++;
  }

  switchState = digitalRead(switchPin); // reads the state of the tilt switch to see if it's in a different position

  if(switchState != prevSwitchState) {
    for(int x = 2; x < 8; x++) { // writes all LEDs low if the switch state has changed
      digitalWrite(x, LOW);
    }

    led = 2; // reset the LEDs to start at pin 2
    previousTime = currentTime; // start counting again
  }

  prevSwitchState = switchState; // reset state

}
